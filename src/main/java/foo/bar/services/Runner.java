/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foo.bar.services;

import foo.bar.model.A;
import foo.bar.model.B;
import foo.bar.model.Assignment;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.AssertTrue;

/**
 *
 * @author mmajchra
 */
@Singleton
@Startup
public class Runner {

  @PersistenceContext
  private EntityManager em;

  @PostConstruct
  public void doSomeTests() {
    A a1 = new A(1l);
    A a2 = new A(2l);
    B b1 = new B(1l);
    B b2 = new B(2l);
    B b3 = new B(3l);

    Assignment a1b1 = new Assignment("a1b1", a1, b1);
    Assignment a1b2 = new Assignment("a1b2", a1, b2);
    Set<Assignment> a1assignments = new HashSet<>();
    a1assignments.add(a1b1);
    a1assignments.add(a1b2);
    //Assingmnment is owner
    //1. Save Asignment
    //2. check A, B - Cascade persist on both
    //3. persist Assignment
    a1assignments.stream().forEach(assignment -> save(assignment));
    A a1FromDB = em.createQuery("from entityA a left join fetch a.assignments where a.id = 1", A.class).getSingleResult();
    System.out.println("After save " + a1FromDB.getAssignments().size());

    Assignment a2b1 = new Assignment("a2b1", a2, b1);
    Assignment a2b2 = new Assignment("a2b2", a2, b2);
    Assignment a2b3 = new Assignment("a2b3", a2, b3);
    Set<Assignment> a2assignments = new HashSet<>();
    a2assignments.add(a2b1);
    a2assignments.add(a2b2);
    a2assignments.add(a2b3);
    a2.setAssignments(a2assignments);
    em.persist(a2);
  }

  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
  public void save(Object e) {
    em.persist(e);
    em.flush();
  }
}
