/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foo.bar.model;

import java.io.Serializable;
import java.util.Objects;
import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mmajchra
 */
@Entity
public class Assignment implements Serializable {

  @Id
  @GeneratedValue
  private Long id;

  public Assignment() {
  }

  public Assignment(String name, A a, B b) {
    this.name = name;
    this.a = a;
    this.b = b;
  }

  private String name;

//  @Id
  @ManyToOne(cascade = CascadeType.PERSIST)
  @JoinColumn(name = "a_id")
  private A a;

//  @Id
  @ManyToOne(cascade = CascadeType.PERSIST)
  @JoinColumn(name = "b_id")
  private B b;

  public A getA() {
    return a;
  }

  public void setA(A a) {
    this.a = a;
  }

  public B getB() {
    return b;
  }

  public void setB(B b) {
    this.b = b;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 19 * hash + Objects.hashCode(this.a);
    hash = 19 * hash + Objects.hashCode(this.b);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Assignment other = (Assignment) obj;
    if (!Objects.equals(this.a, other.a)) {
      return false;
    }
    if (!Objects.equals(this.b, other.b)) {
      return false;
    }
    return true;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


}
